/**
 Input: 3 số thực

IF (điều kiện){
    ...
}else if(điều kiện){
    ...
}else if...

 Output: sắp xếp 3 số từ lớn đến bé
 */

document.getElementById('btnSapXep').addEventListener('click', function(){
    var textSapXep = document.getElementById('textSapXep');
    var number1 = document.getElementById("number1").value*1;
    var number2 = document.getElementById("number2").value*1;
    var number3 = document.getElementById("number3").value*1;

    if(number1 >= number2 && number1 >= number3 && number2 >= number3){
        textSapXep.innerHTML=(number1 + " > " + number2 +" > "+number3);
    }
    else if(number1 >= number2 && number1 >= number3 && number3 >= number2){
        textSapXep.innerHTML=(number1 + " > " + number3 +" > "+number2);
    }
    else if(number2 >= number1 && number2 >= number3 && number1 >= number3){
        textSapXep.innerHTML=(number2 + " > " + number1 +" > "+number3);
    }
    else if(number2 >= number1 && number2 >= number3 && number3 >= number1){
        textSapXep.innerHTML=(number2 + " > " + number3 +" > "+number1);
    }
    else if(number3 >= number1 && number3 >= number2 && number1 >= number2){
        textSapXep.innerHTML=(number3 + " > " + number1 +" > "+number2);
    }
    else{
        textSapXep.innerHTML=(number3 + " > " + number2 +" > "+number1);
    }
});

/**
 * INPUT:
    value: B = Bố, M = Mẹ, A=Anh Trai, E = Em Gái;

    Nếu value = "B" => xin chào bố
    Tương Tự...    
    OUTPUT:
    Lời chào phù hợp
 */

document.getElementById('btnLoiChao').addEventListener('click',function(){
    var txtThanhVien = document.getElementById("txtThanhVien").value;
    var textLoiChao = document.getElementById("textLoiChao");

    if(txtThanhVien == 'B'){
        textLoiChao.innerHTML= "Xin Chào Bố!!!";
    }else if(txtThanhVien == 'M'){
        textLoiChao.innerHTML= "Xin Chào Mẹ!!!";
    }else if(txtThanhVien == 'A'){
        textLoiChao.innerHTML= "Xin Chào Anh Trai!!!";
    }else if(txtThanhVien == 'E'){
        textLoiChao.innerHTML= "Xin Chào Em Gái!!!";
    }else if(txtThanhVien == 'Tv'){
        textLoiChao.innerHTML= "Xin Chào Người Lạ nha!!!";
    }
});
/**
 INPUT:
 3 số thực


biến đếm count == 0;
 Nếu số chia 2 dư 0 => chẵn =>(count ++)
 Nếu số chia 2 dư 1 => lẽ =>(3-count)


 OUTPUT:
 có bao nhiêu số chẵn và lẽ
 */

document.getElementById('btnDem').addEventListener('click', function(){
    var textDem = document.getElementById("textDem");
    var so1 = document.getElementById("so1").value*1;
    var so2 = document.getElementById("so2").value*1;
    var so3 = document.getElementById("so3").value*1;
    var count = 0;
    if(so1 %2 ==0){
        count++;
    }
    if(so2%2==0){
        count++;
    }
    if(so3%2==0){
        count++;
    }
    var soLe = 3 - count;
    textDem.innerHTML = "Có " + count + " số chẵn và "+ soLe + " số lẽ";
});
/*
INPUT:
Chiều dài 3 cạnh tam giác

Nếu a = b , b = c , a = c =>đều
    a = b hoặc a = c hoặc b = c => cân
    a^2 = b^2 + c^2 hoặc b^2 = a^2 + c^2 hoặc c^2 = b^2 + a^2 => vuông
OUTPUT:
Dự Đoán tam giác gì (Đều Vuông Cân)

*/
document.getElementById('btnDuDoan').addEventListener('click',function(){
    var textDuDoan = document.getElementById("textDuDoan");
    var canh1 = document.getElementById("canh1").value*1;
    var canh2 = document.getElementById("canh2").value*1;
    var canh3 = document.getElementById("canh3").value*1;
    if(canh1 == canh2 && canh1 == canh3 && canh2 == canh3){
        textDuDoan.innerHTML = "Đây là tam giác ĐỀU."
    }
     else if(canh1 == canh2 || canh1 == canh3 || canh2 == canh3){
        textDuDoan.innerHTML = "Đây là tam giác CÂN."
    }else if(  canh1*canh1 == canh2*canh2 +canh3*canh3
            || canh2*canh2 == canh1*canh1 + canh3*canh3
            || canh3*canh3 == canh1*canh1 +canh2*canh2){
        textDuDoan.innerHTML = "Đây là tam giác VUÔNG."
    }else{
        textDuDoan.innerHTML = "Đây là 1 loại tam giác nào đó!"
    }
});



// BÀI TẬP THÊM

//BÀI 6
document.getElementById('btnNgay').addEventListener('click', function(){
    var textNgay = document.getElementById('textNgay');
    var ngay = 0;
    var thang = document.getElementById('thang').value*1;
    var nam = document.getElementById('nam').value*1;

    if(thang == 2 && nam % 4 == 0 && nam % 100 !=0){
        ngay = 29;
    }else{
        ngay = 28;
    }
    switch(thang){
        case 1 : ngay = 31;break;
        case 3 : ngay = 31;break;
        case 5 : ngay = 31;break;
        case 7 : ngay = 31;break;
        case 8 : ngay = 31;break;
        case 10 : ngay = 31;break;
        case 12 : ngay = 31;break;
        case 4 : ngay = 30;break;
        case 6 : ngay = 30;break;
        case 9 : ngay = 30;break;
        case 11 : ngay = 30;break;
    }
    

    textNgay.innerHTML = "Tháng " + thang + " Năm " + nam + " Có " + ngay +" Ngày ";
})




// BÀI 7
document.getElementById('btnDoc').addEventListener('click',function(){
    var textDoc = document.getElementById('textDoc');
    var so3ChuSo = document.getElementById('so3ChuSo').value*1;
    var hangDonVi = Math.floor(so3ChuSo % 10);
    var hangChuc = Math.floor((so3ChuSo/10) % 10);
    var hangTram = Math.floor(so3ChuSo / 100);
    var textTram = "";    
    var textChuc = "";    
    var textDonVi = "";    
    if(so3ChuSo < 100 || so3ChuSo >999 ){
        textDoc.innerHTML = "Nhập lại số có 3 chữ số"
    }else{
        switch(hangTram){
            case 1 : textTram = "Một Trăm "; break ;
            case 2 : textTram = "Hai Trăm "; break ;
            case 3 : textTram = "Ba Trăm "; break ;
            case 4 : textTram = "Bốn Trăm "; break ;
            case 5 : textTram = "Năm Trăm "; break ;
            case 6 : textTram = "Sáu Trăm "; break ;
            case 7 : textTram = "Bảy Trăm "; break ;
            case 8 : textTram = "Tám Trăm "; break ;
            case 9 : textTram = "Chín Trăm "; break ;
        }
    if(hangChuc % 10 == 0 && hangDonVi != 0){
        textChuc = "Lẻ";
        console.log("chục")
    }else{
        switch(hangChuc){
            case 1 : textChuc = "Mười "; break ;
            case 2 : textChuc = "Hai Mươi "; break ;
            case 3 : textChuc = "Ba Mươi "; break ;
            case 4 : textChuc = "Bốn Mươi "; break ;
            case 5 : textChuc = "Năm Mươi "; break ;
            case 6 : textChuc = "Sáu Mươi "; break ;
            case 7 : textChuc = "Bảy Mươi "; break ;
            case 8 : textChuc = "Tám Mươi "; break ;
            case 9 : textChuc = "Chín Mươi "; break ;
        }
        switch(hangDonVi){
            case 1 : textDonVi = "Một "; break ;
            case 2 : textDonVi = "Hai"; break ;
            case 3 : textDonVi = "Ba"; break ;
            case 4 : textDonVi = "Bốn"; break ;
            case 5 : textDonVi = "Năm"; break ;
            case 6 : textDonVi = "Sáu"; break ;
            case 7 : textDonVi = "Bảy"; break ;
            case 8 : textDonVi = "Tám"; break ;
            case 9 : textDonVi = "Chín"; break ;
        }
    }  
    }
    textDoc.innerHTML = textTram + textChuc + textDonVi;
})


// SHOW HIDE CÁC BÀI TẬP
var btnBai1 = document.getElementById('btnBai1');
var btnBai2 = document.getElementById('btnBai2');
var btnBai3 = document.getElementById('btnBai3');
var btnBai4 = document.getElementById('btnBai4');
var btnBai6 = document.getElementById('btnBai6');
var btnBai7 = document.getElementById('btnBai7');
var group1 = document.getElementById('group1');
var group2 = document.getElementById('group2');
var group3 = document.getElementById('group3');
var group4 = document.getElementById('group4');
var group6 = document.getElementById('group6');
var group7 = document.getElementById('group7');
function toggleGroup1(){
    group1.classList.toggle('hide');
};
function toggleGroup2(){
    group2.classList.toggle('hide');
};
function toggleGroup3(){
    group3.classList.toggle('hide');
};
function toggleGroup4(){
    group4.classList.toggle('hide');
};
function toggleGroup6(){
    group6.classList.toggle('hide');
};
function toggleGroup7(){
    group7.classList.toggle('hide');
};
btnBai1.addEventListener('click',toggleGroup1);
btnBai2.addEventListener('click',toggleGroup2);
btnBai3.addEventListener('click',toggleGroup3);
btnBai4.addEventListener('click',toggleGroup4);
btnBai6.addEventListener('click',toggleGroup6);
btnBai7.addEventListener('click',toggleGroup7);